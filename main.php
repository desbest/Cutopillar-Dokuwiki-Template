<?php
/**
 * Cutopillar Template
 * Based on the starter template
 *
 * @link     http://dokuwiki.org/template:cutopillar
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/mobileinputsize.js"></script>
</head>

<body>


<div id="dokuwiki__top" class="offset site <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">
        <div class="sidebar" id="topmenu">
            <div class="widget-area aside">
                <ul class="xoxo">
                    <li class="widget-container widget_categories" id="categories-7">
                        <!-- <h3 class="widget-title">Categories</h3> -->
                            <div class="padhere" for="cat">&nbsp;<br></div>
                            <?php
                                echo (new \dokuwiki\Menu\MobileMenu())->getDropdown('Wiki Menu');
                            ?>
                    </li>
                </ul>
            </div>
        </div>

        <?php tpl_includeFile('header.html') ?>
        <div class="header">
            <h1 id="blog-title"><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
                <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                        tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
            <?php if ($conf['tagline']): ?>
                    <p id="blog-description" class="claim"><?php echo $conf['tagline'] ?></p>
            <?php endif ?>

            <!-- BREADCRUMBS -->
            <?php if($conf['breadcrumbs']){ ?>
                <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
            <?php } ?>
            <?php if($conf['youarehere']){ ?>
                <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
            <?php } ?>

             <ul class="a11y skip">
                <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
            </ul>
        </div>
        <div class="statrow flex">
        <div class="statcol content sparewidth">

        <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>
        <!-- <div class="content"> -->
            <div class="posthere">
                <!-- <h2 class="entry-title"><a href="=" rel="bookmark" title="Permalink to blog post">Blog post title</a></h2>
                <div class="entry-meta">
                    <!-- <span class="meta-prep meta-prep-author">Posted on</span> -->
                    <!-- <a href="">August 11, 2012</a> by <a href="#">desbest</a>
                </div> -->
                <div class="readingcontent">

                   <!-- ********** CONTENT ********** -->
                    <div id="dokuwiki__content"><div class="pad">
                        <?php tpl_flush() /* flush the output buffer */ ?>
                        <?php tpl_includeFile('pageheader.html') ?>

                        <div class="page">
                            <!-- wikipage start -->
                            <?php tpl_content() /* the main content */ ?>
                            <!-- wikipage stop -->
                            <div class="clearer"></div>
                        </div>

                        <?php tpl_flush() ?>
                        <?php tpl_includeFile('pagefooter.html') ?>
                    </div></div><!-- /content -->
                   
                    <!-- <p><a class="more-link" href="http://localhost/prowler2/2012/08/a-history-of-facebook-designs-part-3/#more-491">Continue reading &rarr;</a></p> -->
                </div>
                <!-- <ul class="xoxo">
                    <li class="widget-container widget_text" id="text-5">
                        <div class="textwidget"></div>
                    </li>
                </ul> -->
                <!-- <div class="entry-utility">
                    Posted in <a href="">Design</a> | <a href="">1 Comment</a>
                </div> -->
            </div>
            <!-- <div class="padbelow">
                <a href="#">&larr; Older posts</a>
            </div> -->
            <!-- <ul class="xoxo">
                <li class="widget-container widget_text" id="text-7">
                    <div class="textwidget"></div>
                </li>
            </ul> -->
            <!--wp_footer-->
        <!-- </div> -->
        </div>
      
              
            <!-- ********** ASIDE ********** -->
            <?php if ($showSidebar): ?>
          <div class="statcol">
            <div class="realsidebar">
                <div id="writtensidebar">
                    <?php tpl_includeFile('sidebarheader.html') ?>
                    <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                    <?php tpl_includeFile('sidebarfooter.html') ?>
                    <div class="clearer"></div>
                </div><!-- /aside -->
                 <!-- <p>Here is the sidebar.<br><br></p> -->

            </div>
            </div>
            <?php endif; ?>

             
        </div>
        </div> <!-- end statrow -->
        <div class="statrow">
        <div class="sidebar" style="width: 90%; margin-left: auto; margin-right: auto;">
            <p>Cutopillar theme <a href="http://desbest.com">designed by desbest</a></p>
            <div class="doc"><p><?php tpl_pageinfo() /* 'Last modified' etc */ ?></p></div>
            <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
            <?php tpl_includeFile('footer.html') ?>
        </div>
        <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
    </div>



    <?php /* with these Conditional Comments you can better address IE issues in CSS files,
             precede CSS rules by #IE8 for IE8 (div closes at the bottom) */ ?>
    <!--[if lte IE 8 ]><div id="IE8"><![endif]-->

    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>
        
        

    


         
   
        

    
    
</body>
</html>
