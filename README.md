# Cutopillar dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:cutopillar)

![cutopillar theme screenshot](https://i.imgur.com/DH9JkOk.png)
